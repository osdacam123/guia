


<?php
/*****6.ESCRIBA UNA FUNCIÓN QUE RECIBA COMO PARÁMETRO UN ARREGLO CON EL NOMBRE Y TRES NOTAS DE 10 ESTUDIANTES, 
LA SALIDA DEBERÁ MOSTRAR:

A.NOMBRES Y NOTAS DE LOS ESTUDIANTES.

B.EL PROMEDIO DE LAS NOTAS POR ESTUDIANTE.

C.SI LA NOTA ES MAYOR A 7, DEBERÁ MOSTRAR EL MENSAJE “APROBADO” DE LO CONTRARIO DEBERÁ MOSTRAR “DEBE MEJORAR”. */


  $datos = array(
    array(
        "nombre" => "Daniel Campos",
        "not1" => 5,
        "not2" => 8,  
        "not3" => 9, 
),
array(
    "nombre" => "Karina Perez",
    "not1" => 10,
    "not2" => 10,  
    "not3" => 5, 
),
array(
    "nombre" => "Victoria Hernandez",
    "not1" => 5,
    "not2" => 8,  
    "not3" => 9, 
),
array(
    "nombre" => "Elida Campos",
    "not1" => 9,
    "not2" => 8,  
    "not3" => 5, 
),
array(
    "nombre" => "Edwin Josue",
    "not1" => 10,
    "not2" => 8,  
    "not3" => 9, 
),
array(
    "nombre" => "Diana Gonzalez",
    "not1" => 6,
    "not2" => 8,  
    "not3" => 6, 
),
array(
    "nombre" => "Carlos Garcia",
    "not1" => 5,
    "not2" => 5,  
    "not3" => 6, 
),
array(
    "nombre" => "Miguel Rodriguez",
    "not1" => 10,
    "not2" => 7,  
    "not3" => 4, 
),
array(
    "nombre" => "Daniel Alcides",
    "not1" => 10,
    "not2" => 9,  
    "not3" => 3, 
),
array(
    "nombre" => "Sol Garrido",
    "not1" => 10,
    "not2" => 10,  
    "not3" => 2, 
)

);

    function not($notas){

        foreach($notas as $key)
 	    {
         $prommedio = ($key['not1'] + $key['not2'] + $key['not3']) / 3;
         $estado = "";
         if($prommedio >= 7){
             $estado = "Aprobado";
         }else{
             $estado = "Debe mejorar";
         }
            echo "<tr>";
            echo "<td>" .$key['nombre'] ."</td>";
            echo "<td>" .$key['not1'] ."</td>";
            echo "<td>" .$key['not2'] ."</td>";
            echo "<td>" .$key['not3'] ."</td>";
            echo "<td>" .$prommedio . "</td>";
            echo "<td>" .$estado . "</td>";
 	        echo "<tr>";
 	    }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style/style.css">
    <title>NOTAS</title>
</head>
<body>
    
 <table border="2">
 <tr>
                <th>Nombre</th>
                <th>Nota 1</th>
                <th>Nota 2</th>
                <th>Nota 3</th>  
                <th>Promedio</th>
                <th>Estado</th>
            </tr>
 <?php
    not($datos);
?>
           
           </table>
    </div>
</body>
</html>